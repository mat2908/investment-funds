**Investment Funds Calculator** 

To run application:

`./gradlew build`

Application can be run only from unit tests level.


Application is just simple business logic and many things should be added to it if we would like to use it in production, 
eg. 

- logging


- encapsulate some types, for example `BigDecimal`


- probably some optimizations should be possible


Application is not able to handle undistributed amount.