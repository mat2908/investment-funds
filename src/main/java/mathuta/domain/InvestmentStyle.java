package mathuta.domain;

import java.math.BigDecimal;

public interface InvestmentStyle {
    BigDecimal getPercentage(InvestmentFundKind fundKind);
}
