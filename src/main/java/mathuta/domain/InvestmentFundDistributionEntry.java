package mathuta.domain;

import java.math.BigDecimal;

public class InvestmentFundDistributionEntry {
    private InvestmentFund fund;
    private BigDecimal percentage;

    public InvestmentFundDistributionEntry(InvestmentFund fund, BigDecimal percentage) {
        this.fund = fund;
        this.percentage = percentage;
    }

    public InvestmentFund getFund() {
        return fund;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }
}
