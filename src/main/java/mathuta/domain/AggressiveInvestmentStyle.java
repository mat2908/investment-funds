package mathuta.domain;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static mathuta.domain.InvestmentFundKind.FOREIGN;
import static mathuta.domain.InvestmentFundKind.MONETARY;
import static mathuta.domain.InvestmentFundKind.POLISH;

public class AggressiveInvestmentStyle implements InvestmentStyle {

    private Map<InvestmentFundKind, BigDecimal> fundPercentageDistribution = new HashMap<InvestmentFundKind, BigDecimal>() {{
        put(POLISH, new BigDecimal(40));
        put(FOREIGN, new BigDecimal(20));
        put(MONETARY, new BigDecimal(40));
    }};

    @Override
    public BigDecimal getPercentage(InvestmentFundKind fundKind) {
        return fundPercentageDistribution.getOrDefault(fundKind, BigDecimal.ZERO);
    }
}