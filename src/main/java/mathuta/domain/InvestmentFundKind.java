package mathuta.domain;

public enum InvestmentFundKind {
    POLISH, FOREIGN, MONETARY
}
