package mathuta.domain;

import java.math.BigDecimal;

public class CalculatedInvestmentFund {
    private InvestmentFund investmentFund;
    private BigDecimal amount;
    private BigDecimal percentage;

    public CalculatedInvestmentFund(InvestmentFund investmentFund, BigDecimal amount, BigDecimal percentage) {
        this.investmentFund = investmentFund;
        this.amount = amount;
        this.percentage = percentage;
    }

    public InvestmentFund getInvestmentFund() {
        return investmentFund;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }


}
