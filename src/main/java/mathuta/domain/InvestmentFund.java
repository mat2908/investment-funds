package mathuta.domain;

public class InvestmentFund {
    private String id;
    private String name;
    private InvestmentFundKind fundKind;

    public InvestmentFund(String id, String name, InvestmentFundKind fundKind) {
        this.id = id;
        this.name = name;
        this.fundKind = fundKind;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public InvestmentFundKind getFundKind() {
        return fundKind;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvestmentFund fund = (InvestmentFund) o;

        return id != null ? id.equals(fund.id) : fund.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
