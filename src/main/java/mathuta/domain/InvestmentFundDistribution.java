package mathuta.domain;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;

public class InvestmentFundDistribution {
    private final List<InvestmentFund> funds;
    private final InvestmentStyle investmentStyle;
    private final Balance balance;

    public InvestmentFundDistribution(BigDecimal amount, List<InvestmentFund> funds, InvestmentStyle investmentStyle) {
        this.balance = new Balance(amount);
        this.funds = funds;
        this.investmentStyle = investmentStyle;
    }

    public List<CalculatedInvestmentFund> calculateDistribution() {
        Map<InvestmentFundKind, List<InvestmentFund>> fundsByKind = funds.stream()
                .collect(groupingBy(InvestmentFund::getFundKind, mapping(Function.identity(), toList())));

        return fundsByKind.entrySet().stream()
                .map(e -> distributeInvestmentFunds(e.getValue(), investmentStyle.getPercentage(e.getKey())))
                .flatMap(Collection::stream)
                .map(d -> new CalculatedInvestmentFund(d.getFund(), balance.charge(d.getPercentage()), d.getPercentage()))
                .collect(toList());
    }

    public BigDecimal getUndistributedAmount() {
        return balance.getBalance();
    }

    private List<InvestmentFundDistributionEntry> distributeInvestmentFunds(List<InvestmentFund> singleKindFunds, BigDecimal fundKindPercentage) {
        BigDecimal[] divisionResult = calculatePercentageWithRemainderForSingleFund(singleKindFunds.size(), fundKindPercentage);

        Optional<InvestmentFund> reminderFund = singleKindFunds.stream().findFirst();
        if (!reminderFund.isPresent()) {
            return Collections.emptyList();
        }

        return singleKindFunds.stream()
                .map(fund -> fund.equals(reminderFund.get())
                        ? new InvestmentFundDistributionEntry(fund, divisionResult[0].add(divisionResult[1]))
                        : new InvestmentFundDistributionEntry(fund, divisionResult[0]))
                .collect(toList());
    }

    private BigDecimal[] calculatePercentageWithRemainderForSingleFund(int numberOfFunds, BigDecimal kindPercentage) {
        BigDecimal funds = new BigDecimal(numberOfFunds);
        BigDecimal divideResult = kindPercentage.divide(funds, 2, BigDecimal.ROUND_DOWN);
        BigDecimal subtractResult = BigDecimal.ZERO;

        if (!divideResult.multiply(funds).equals(kindPercentage)) {
            subtractResult = kindPercentage.subtract(divideResult.multiply(funds));
        }

        return new BigDecimal[] {divideResult, subtractResult};
    }
}
