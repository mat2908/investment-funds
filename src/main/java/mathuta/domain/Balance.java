package mathuta.domain;

import java.math.BigDecimal;

public class Balance {
    private final BigDecimal initialValue;
    private BigDecimal chargeableValue;

    public Balance(BigDecimal initialValue) {
        if (initialValue.compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Initial value cannot have negative value");
        }

        this.initialValue = initialValue;
        this.chargeableValue = new BigDecimal(initialValue.toString());
    }

    public BigDecimal charge(BigDecimal percentage) {
        BigDecimal divideResult = initialValue.multiply(percentage).divide(new BigDecimal(100), 2, BigDecimal.ROUND_UP);
        chargeableValue = chargeableValue.subtract(divideResult); //TODO I suppose here should be some logic that calculates undistributed amount
        return divideResult;
    }

    public BigDecimal getBalance() {
        return chargeableValue;
    }
}
