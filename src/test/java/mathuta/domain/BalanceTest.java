package mathuta.domain;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class BalanceTest {

    @Test
    public void shouldChargeGivenPercentageFromTotalBalance() {
        Balance balance = new Balance(new BigDecimal(200));

        BigDecimal result = balance.charge(new BigDecimal(10));

        assertThat("Should return 10% of initial amount", result.intValue(), is(equalTo(20)));
        assertThat("Balance should be less for 10%", balance.getBalance().intValue(), is(equalTo(180)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionWhenInitialAmountIsLessThanZero() {
        Balance balance = new Balance(new BigDecimal(-1));
    }
}