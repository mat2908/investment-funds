package mathuta.domain;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class InvestmentFundDistributionTest {

    @Test
    public void shouldCalculateInvestmentFundDistributionForSafeStyle() {
        List<InvestmentFund> investmentFunds = Arrays.asList(
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Polski 1", InvestmentFundKind.POLISH),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Polski 2", InvestmentFundKind.POLISH),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Zagraniczny 1", InvestmentFundKind.FOREIGN),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Zagraniczny 2", InvestmentFundKind.FOREIGN),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Zagraniczny 3", InvestmentFundKind.FOREIGN),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Pieniezny 1", InvestmentFundKind.MONETARY));
        BigDecimal amount = BigDecimal.valueOf(10000L);

        InvestmentFundDistribution underTest = new InvestmentFundDistribution(amount, investmentFunds, new SafeInvestmentStyle());

        List<CalculatedInvestmentFund> calculatedInvestmentFunds = underTest.calculateDistribution();
        assertTrue(DoubleStream.of(1000, 1000, 2500, 2500, 2500, 500)
                .allMatch(i -> calculatedInvestmentFunds.stream()
                        .map(CalculatedInvestmentFund::getAmount)
                        .map(BigDecimal::doubleValue)
                        .anyMatch(j -> j.equals(i))));

        assertTrue(IntStream.of(10, 10, 25, 25, 25, 5)
                .allMatch(i -> calculatedInvestmentFunds.stream()
                        .map(p -> p.getPercentage().intValue())
                        .anyMatch(j -> j.equals(i))));
    }

    @Test
    public void shouldCalculateInvestmentFundDistributionForAggressiveStyle() {
        List<InvestmentFund> investmentFunds = Arrays.asList(
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Polski 1", InvestmentFundKind.POLISH),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Zagraniczny 1", InvestmentFundKind.FOREIGN),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Pieniezny 1", InvestmentFundKind.MONETARY));
        BigDecimal amount = BigDecimal.valueOf(10000L);

        InvestmentFundDistribution underTest = new InvestmentFundDistribution(amount, investmentFunds, new AggressiveInvestmentStyle());

        List<CalculatedInvestmentFund> calculatedInvestmentFunds = underTest.calculateDistribution();
        assertTrue(DoubleStream.of(4000, 2000, 4000)
                .allMatch(i -> calculatedInvestmentFunds.stream()
                        .map(CalculatedInvestmentFund::getAmount)
                        .map(BigDecimal::doubleValue)
                        .anyMatch(j -> j.equals(i))));

        assertTrue(IntStream.of(40, 20, 40)
                .allMatch(i -> calculatedInvestmentFunds.stream()
                        .map(p -> p.getPercentage().intValue())
                        .anyMatch(j -> j.equals(i))));
    }

    @Test
    public void shouldCalculateInvestmentFundDistributionForBalancedStyle() {
        List<InvestmentFund> investmentFunds = Arrays.asList(
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Polski 1", InvestmentFundKind.POLISH),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Polski 2", InvestmentFundKind.POLISH),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Zagraniczny 1", InvestmentFundKind.FOREIGN),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Zagraniczny 2", InvestmentFundKind.FOREIGN),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Zagraniczny 3", InvestmentFundKind.FOREIGN),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Pieniezny 1", InvestmentFundKind.MONETARY));
        BigDecimal amount = BigDecimal.valueOf(10000L);

        InvestmentFundDistribution underTest = new InvestmentFundDistribution(amount, investmentFunds, new BalancedInvestmentStyle());

        List<CalculatedInvestmentFund> calculatedInvestmentFunds = underTest.calculateDistribution();
        assertTrue(DoubleStream.of(1500, 1500, 2000, 2000, 2000, 1000)
                .allMatch(i -> calculatedInvestmentFunds.stream()
                        .map(CalculatedInvestmentFund::getAmount)
                        .map(BigDecimal::doubleValue)
                        .anyMatch(j -> j.equals(i))));

        assertTrue(IntStream.of(15, 15, 20, 20, 20, 10)
                .allMatch(i -> calculatedInvestmentFunds.stream()
                        .map(p -> p.getPercentage().intValue())
                        .anyMatch(j -> j.equals(i))));
    }

    @Test
    @Ignore
    public void shouldCalculateInvestmentFundDistributionForSafeStyleAndReturnUndistributedAmount() {
        List<InvestmentFund> investmentFunds = Arrays.asList(
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Polski 1", InvestmentFundKind.POLISH),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Polski 2", InvestmentFundKind.POLISH),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Zagraniczny 1", InvestmentFundKind.FOREIGN),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Zagraniczny 2", InvestmentFundKind.FOREIGN),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Zagraniczny 3", InvestmentFundKind.FOREIGN),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Pieniezny 1", InvestmentFundKind.MONETARY));
        BigDecimal amount = BigDecimal.valueOf(10001L);

        InvestmentFundDistribution underTest = new InvestmentFundDistribution(amount, investmentFunds, new SafeInvestmentStyle());

        List<CalculatedInvestmentFund> calculatedInvestmentFunds = underTest.calculateDistribution();
        assertTrue(DoubleStream.of(1000, 1000, 2500, 2500, 2500, 500)
                .allMatch(i -> calculatedInvestmentFunds.stream()
                        .map(CalculatedInvestmentFund::getAmount)
                        .map(BigDecimal::doubleValue)
                        .anyMatch(j -> j.equals(i))));

        assertTrue(IntStream.of(10, 10, 25, 25, 25, 5)
                .allMatch(i -> calculatedInvestmentFunds.stream()
                        .map(p -> p.getPercentage().intValue())
                        .anyMatch(j -> j.equals(i))));

        assertThat(underTest.getUndistributedAmount(), is(equalTo(1)));
    }

    @Test
    public void shouldCalculateInvestmentFundDistributionForSafeStyleWithoutUndistributedAmount() {
        List<InvestmentFund> investmentFunds = Arrays.asList(
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Polski 1", InvestmentFundKind.POLISH),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Polski 2", InvestmentFundKind.POLISH),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Polski 3", InvestmentFundKind.POLISH),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Zagraniczny 1", InvestmentFundKind.FOREIGN),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Zagraniczny 2", InvestmentFundKind.FOREIGN),
                new InvestmentFund(UUID.randomUUID().toString(), "Fundusz Pieniezny 1", InvestmentFundKind.MONETARY));
        BigDecimal amount = BigDecimal.valueOf(10000L);

        InvestmentFundDistribution underTest = new InvestmentFundDistribution(amount, investmentFunds, new SafeInvestmentStyle());

        List<CalculatedInvestmentFund> calculatedInvestmentFunds = underTest.calculateDistribution();
        assertTrue(DoubleStream.of(668, 666, 666, 3750, 3750, 500)
                .allMatch(i -> calculatedInvestmentFunds.stream()
                        .map(CalculatedInvestmentFund::getAmount)
                        .map(BigDecimal::doubleValue)
                        .anyMatch(j -> j.equals(i))));

        assertTrue(DoubleStream.of(6.68, 6.66, 6.66, 37.5, 37.5, 5)
                .allMatch(i -> calculatedInvestmentFunds.stream()
                        .map(p -> p.getPercentage().doubleValue())
                        .anyMatch(j -> j.equals(i))));

        assertThat(underTest.getUndistributedAmount().intValue(), is(equalTo(0)));
    }
}